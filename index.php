<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    require("animals.php");
    require("ape.php");
    require("frog.php");

    // 
    $sheep = new Animal("shaun", 4, "no");
    $monyet = new ape("kera sakti", 2, "no", "auooo");
    $katak = new frog("buduk", 4, "no", "Hop-hop");
    echo "name :$sheep->name "; // "shaun"
    echo "<br>";
    echo "legs :$sheep->legs "; // 4
    echo "<br>";
    echo  "cold blooded :$sheep->cold_blooded "; // "no"
    echo "<br>";
    echo "<br>";
    echo "name :$monyet->name "; // "shaun"
    echo "<br>";
    echo "legs :$monyet->legs "; // 4
    echo "<br>";
    echo  "cold blooded :$monyet->cold_blooded "; // "no"
    echo "<br>";
    echo "Yell :" . $monyet->getbunyi() . "<br>";

    echo "<br>";
    echo "name :$katak->name "; // "shaun"
    echo "<br>";
    echo "legs :$katak->legs "; // 4
    echo "<br>";
    echo  "cold blooded :$katak->cold_blooded "; // "no"
    echo "<br>";
    echo "Yell :" . $katak->getjump() . "<br>";
    ?>
</body>

</html>


<!-- // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded()) -->