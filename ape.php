<?php
class ape extends animal
{
    private $bunyi;
    public function __construct($name = "name", $legs = "legs", $cold_blooded = "cold blooded", $bunyi = "bunyi")
    {
        parent::__construct($name, $legs, $cold_blooded);
        $this->bunyi = $bunyi;
    }
    public function setbunyi($bunyi)
    {
        $this->bunyi = $bunyi;
    }
    public function getbunyi()
    {
        return $this->bunyi;
    }
}
