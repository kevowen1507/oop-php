<?php
class animal {
    public $name,
           $legs,
           $cold_blooded;

    public function __construct($name = "name",$legs = "legs",$cold_blooded = "cold_blooded"){
           $this->name = $name;
           $this ->legs = $legs;
           $this ->cold_blooded = $cold_blooded;
    }
}
