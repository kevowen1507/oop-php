<?php
class frog extends animal
{
    private $jump;
    public function __construct($name = "name", $legs = "legs", $cold_blooded = "cold blooded", $jump = "jump")
    {
        parent::__construct($name, $legs, $cold_blooded);
        $this->jump = $jump;
    }
    public function setjump($jump)
    {
        $this->jump = $jump;
    }
    public function getjump()
    {
        return $this->jump;
    }
}
